# README #

In this task work implement app with list of movies 

### Implemented features ###

* List of movies(data take from api)
* Sort
* Infinite scroll
* Modal with more information

### How do I get set up? ###

* clone repo
* run `npm i`
* for a dev build run `npm run dev`

### Additional ###

In this test work i tried to use technologies what i don't use every day on my job, but i tried to implement best practice everywhere