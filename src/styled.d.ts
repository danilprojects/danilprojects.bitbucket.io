import 'styled-components'
import Footer from './components/Footer';

declare module 'styled-components' {
  export interface DefaultTheme {
    borderRadius: string

    colors: {
        header: string
        page: string
        Footer: string
    }
  }
}