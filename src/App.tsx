import React from 'react';
import Header from './components/Header';
import Movies from './components/Movies';
import Footer from './components/Footer';
import Modal from './components/common/Modal';
import connector from './AppConnector'
import { GlobalStyle, Container, AppContainer } from './style';

type AppProps = {
  isModalVisible?: boolean
}

const App: React.FC<AppProps> = ({ isModalVisible }) => {
  return (
    <>
      <GlobalStyle />
      <AppContainer>
        <Header />
        <Container>
          <Movies />
        </Container>
        <Footer />
      </AppContainer>
      {
        isModalVisible && (
          <Modal />
        )
      }
    </>
  );
}

export default connector(App);
