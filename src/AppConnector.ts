import { connect } from 'react-redux';
import { RootState } from './app/store';

const isModalVisibleSelector = (state: RootState) => state.selectedMovie.isModalVisible;

const mapStateToProps = (state: RootState) => ({
    isModalVisible: isModalVisibleSelector(state)
});

const connector = connect(mapStateToProps, null)

export default connector;