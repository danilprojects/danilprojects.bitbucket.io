import styled from 'styled-components';
import { colors } from '../../style';

export const FooterStlye = styled.div`
    width: 100%;
    height: 60px;
    color: #272626;
    background: ${ colors.footer };
    padding: 20px;
` 