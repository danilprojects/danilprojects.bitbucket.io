import { connect } from 'react-redux';
import { setSort } from '../Movies/MovieSlice';

const mapDispatchToProps = {
    setSort: (sortBy: string) => setSort(sortBy)
}

const connector = connect(null, mapDispatchToProps)

export default connector;