import styled from 'styled-components';
import { colors } from '../../style';

export const HeaderContainer = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%;
    padding: 20px;
    background: ${ colors.header };

    .select {
        width: 100px;
    }
`
export const Title = styled.div`
    color: #fff;
    font-size: 24px;
    line-height: 33px;
`