import React, { useState } from 'react';
import Select, { ValueType } from 'react-select';
import connector from './HeaderConnector';

import { HeaderContainer, Title } from './style';

type SelectedOptionsType = {
    label: string,
    value: string
}

type HeaderProps = {
    setSort: (selectedOption: string) => void
}

const Header: React.FC<HeaderProps> = ({ setSort }) => {
    const options = [
        { value: 'asc', label: 'A-Z' },
        { value: 'desc', label: 'Z-A' },
        { value: 'upper', label: 'Rating ^' },
        { value: 'less', label: 'Rating ˅' }
    ];
    
    const [selectedValue, setSelectedValue] = useState<ValueType<SelectedOptionsType> | null>(null)

    const handleChange = (selectedOption: ValueType<SelectedOptionsType>) => {    
        if (selectedOption) {
            const value = (selectedOption as SelectedOptionsType).value;

            setSelectedValue(selectedOption);
            setSort(value)
        }
    }

    return (
        <HeaderContainer>
            <Title>
                Movies
            </Title>
            <Select
                className="select"
                placeholder="Sort"
                value={selectedValue}
                onChange={(option: ValueType<SelectedOptionsType>) => handleChange(option)}
                options={options}
            />
        </HeaderContainer>
    )
};

export default connector(Header);