import { connect } from 'react-redux';
import { createSelector } from "reselect";
import { setModalInviseble } from '../Movie/MovieSlice';
import { RootState } from '../../../app/store';

type MovieType = {
    id: number
}

const isModalVisibleSelector = (state: RootState) => state.selectedMovie.isModalVisible;
const selectedMovieIdSelector = (state: RootState) => state.selectedMovie.selectedMovieId;
const moviesSelector = (state: RootState): any => state.movies;

const getSelectedMovie = createSelector(
    [moviesSelector, selectedMovieIdSelector],
    (movies, selectedMovieId: number) => (movies.data && movies.data.length) ? movies.data.filter((movie: MovieType) => movie.id === selectedMovieId) : []
);


const mapStateToProps = (state: RootState) => ({
    isModalVisible: isModalVisibleSelector(state),
    selectedMovie: getSelectedMovie(state),
});

const mapDispatchToProps = {
    setModalInviseble
}

const connector = connect(mapStateToProps, mapDispatchToProps)

export default connector;