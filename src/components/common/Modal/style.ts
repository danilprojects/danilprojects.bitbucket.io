import styled from 'styled-components';
import { device } from '../../../style';

export const ModalContainer = styled.div`
    position: fixed;
    background-color: rgba(46, 43, 43, 0.7);
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 2;
    opacity: 0;
    pointer-events: none;
    transition: all 0.3s;

    &.visible {
        opacity: 1;
        pointer-events: all;
    }
`

export const ModalContent = styled.div`
    width: 60%;
    max-width: 650px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    padding: 2em;
    background: #1A1919;

    @media ${ device.tablet } {
        width: 90%;
    }
`

export const Close = styled.button`
    color: #fff;
    font-size: 80%;
    position: absolute;
    right: 0;
    text-align: center;
    cursor: pointer;
    top: 10px;
    border: none;
    background: unset;
    width: 70px;
    text-decoration: none;

    &:hover {
        color: black;
    }
`

export const Image = styled.img`
    width: 100%;
    height: 100%;
`

export const Content = styled.div`
    display: flex;

    @media ${device.mobileL} {
        flex-direction: column;
    }

    .imageContainer {
        width: 300px;

        @media ${device.mobileL} {
            width: 100%;
        }
    }

    .textContainer {
        width: 100%;
        padding-left: 20px;

        .title {
            text-align: center;
            color: #fff;
        }

        .description {
            color: #fff;
        }
    }

    .generaInformation {
        display: flex;
        justify-content: space-between;
        color: #dadada;
        font-size: 14px;
    }
`