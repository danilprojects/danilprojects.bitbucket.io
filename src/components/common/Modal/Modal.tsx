import React, { useState, useEffect } from 'react';
import connetor from './ModalConnector';
import { IMAGE_URL } from '../../../constants';

import { ModalContainer, ModalContent, Close, Content, Image } from './style';

type ModalProps = {
    selectedMovie: Array<MovieType>,
    isModalVisible?: boolean,
    setModalInviseble?: () => void
}

type MovieType = {
    title: string,
    release_date: string,
    poster_path: string,
    vote_average: number,
    vote_count: number,
    overview: string,
    original_language: string
}

const Modal: React.FC<ModalProps> = ({ selectedMovie, isModalVisible, setModalInviseble }) => {
    const [image, setImage] = useState<string | undefined>(undefined)
    const [voteAverageColor, setVoteAverageColor] = useState<string | undefined>(undefined)
    const {
        title,
        release_date,
        poster_path,
        vote_average,
        vote_count,
        overview,
        original_language
    }: MovieType = selectedMovie[0];

    useEffect(() => {
        if ( vote_average === 0) {
            setVoteAverageColor('#dadada');
            return;
        }

        if (vote_average <= 3) {
            setVoteAverageColor('red');
            return;
        }

        if (vote_average <= 5) {
            setVoteAverageColor('yellow');
            return;
        }

        setVoteAverageColor('green');
    }, [vote_average])

    useEffect(() => {
        if (!poster_path) {
            setImage('/no_image.jpg');
            return;
        }

        setImage(`${IMAGE_URL}${poster_path}`);
    }, [poster_path])

    return (
        <ModalContainer className={isModalVisible ? 'visible' : ''}>
            <ModalContent>
                <Close onClick={setModalInviseble}>Close</Close>
                <Content>
                    <div className="imageContainer">
                        <Image src={image} alt={title} />
                    </div>
                    <div className="textContainer">
                        <h2 className="title">{`${title} (${release_date})`}</h2>
                        <p className="description">
                            {
                                overview
                            }
                        </p>
                        <div className="generaInformation">
                            <span className="voteAverage">Average appraisal: <span style={{ color: voteAverageColor }}>{vote_average}</span></span>
                            <span className="voteCount">Vote: {vote_count ? vote_count : 'no votes'}</span>
                            <span className="language">Original language: {original_language}</span>
                        </div>
                    </div>
                </Content>
            </ModalContent>
        </ModalContainer>
    )
}

export default connetor(Modal);