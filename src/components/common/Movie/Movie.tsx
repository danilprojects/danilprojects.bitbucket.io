import React, { useMemo, useState } from 'react';
import connector from './MovieConnector';
import { IMAGE_URL } from '../../../constants';

import { MovieContainer, Image, Mask, Content } from './style';

type MovieProps = {
    imgPath: string,
    overview: string,
    title: string,
    id: number,
    onButtonClick: (id: number) => void
}

const Movie = ({ imgPath, overview, title, id, onButtonClick }: MovieProps) => {
    const [image, setImage] = useState<string | undefined>(undefined);

    const handleClick = () => {
        onButtonClick(id);
    }

    useMemo(() => {
        if (!imgPath) {
            setImage('/no_image.jpg');
            return;
        }

        setImage(`${IMAGE_URL}${imgPath}`);   
    }, [imgPath])

    return (
        <MovieContainer>
            <Mask />
            <Content>
                <h2 className='title'>{ title }</h2>
                <p className='text'>{ overview }</p>
                <button onClick={ handleClick } className='button'>Learn More</button>
            </Content>
            <Image src={ image } alt={ title }/>
        </MovieContainer>
    )
}

export default connector(Movie);