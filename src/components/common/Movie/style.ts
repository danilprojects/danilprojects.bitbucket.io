import styled from 'styled-components';
import { device } from '../../../style';

export const MovieContainer = styled.div`
    display: inline-block;
    position: relative;
    background: #181818;
    margin: 0 10px 10px 0;
    flex: 0 1 250px;

    @media ${ device.mobileL } {
        margin: 0 0 10px 0;
    }
`

export const Image = styled.img`
    width: 100%;
    height: 100%;
`
export const Mask = styled.div`
    position: absolute;
    width: 100%;
    height: 100%;
    opacity: 0;
    transition: all 0.4s ease-in-out;

    &:hover {
        background-color: rgba(46, 43, 43, 0.7);
        opacity: 1;
    }
`

export const Content = styled.div`
    width: 100%;
    height: 100%;
    position: absolute;
    overflow: hidden;
    padding: 10px;
    top: 0;
    left: 0;
    opacity: 0;
    background-color: rgba(46, 43, 43, 0.7);
    transition: all 0.4s ease-in-out;

    .title {
        color: #fff;
        text-align: center;
        position: relative;
        font-size: 17px;
        margin: 20px 0 0 0
        transform: translateY(-100px);
        opacity: 0;
        font-family: Raleway, serif;
        transition: all 0.2s ease-in-out; 
    }

    .text {
        max-height: 210px;
        font-size: 14px;
        position: relative;
        color: #fff;
        padding: 0px 10px 0px;
        text-align: center
        transform: translateY(-100px);
        opacity: 0;
        font-family: Raleway, serif;
        transition: all 0.2s ease-in-out;
        overflow: hidden;
    }

    .button {
        width: 100%;
        flex: 1 1 auto;
        padding: 20px;
        border: 2px solid #f7f7f7;
        text-align: center;
        background: unset;
        color: #f7f7f7;
        text-transform: uppercase;
        position: relative;
        overflow: hidden;
        transition: .3s;

        &:after {
            position: absolute;
            transition: .3s;
            content: '';
            width: 0;
            left: 50%;
            bottom: 0;
            height: 3px;
            background: #f7f7f7;
            height: 120%;
            left: -10%;
            transform: skewX(15deg);
            z-index: -1;
        }

        &:hover {
            color: #181818;
            &:after{
                left: -10%;
                width: 120%;
            }
        }
        box-shadow: 0 0 1px #000
        opacity: 0;
        transition: all 0.2s ease-in-out;
    }

    &:hover {
        opacity: 1;

        .title, .button, .text {
            opacity: 1;
            transform: translateY(0px);
        }
    }

`