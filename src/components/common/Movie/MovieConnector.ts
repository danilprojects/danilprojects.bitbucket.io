import { connect } from 'react-redux';
import { setModalVisible } from './MovieSlice';
import { RootState } from '../../../app/store';

const getMovies = (state: RootState) => state.movies.data;

const mapStateToProps = (state: RootState) => ({
    moviesList: getMovies(state)
});

const mapDispatchToProps = {
    onButtonClick: (id: number) => setModalVisible(id)
}

const connector = connect(mapStateToProps, mapDispatchToProps)

export default connector;