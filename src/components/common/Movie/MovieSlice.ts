import { createSlice } from '@reduxjs/toolkit';
import { AppDispatch } from '../../../app/store';

type movieState = {
    isModalVisible: boolean;
    selectedMovieId: number | null
}

const initialState: movieState = {
    isModalVisible: false,
    selectedMovieId: null
};

export const movieSlice = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    setIsModalVisible: (state, action) => {
      state.isModalVisible = action.payload;
    },
    setSelectedMovieId: (state, action) => {
        state.selectedMovieId = action.payload
    }
  },
});

const { setIsModalVisible, setSelectedMovieId } = movieSlice.actions;

export const setModalVisible = (id: number) => (dispatch: AppDispatch) => {
    dispatch(setIsModalVisible(true))
    dispatch(setSelectedMovieId(id))
}

export const setModalInviseble = () => (dispatch: AppDispatch) => dispatch(setIsModalVisible(false));

export default movieSlice.reducer;