import { createSlice } from '@reduxjs/toolkit';
import { fetchMovies } from '../../api/fetchMovies';
import { AppDispatch } from '../../app/store';
import { INITIAL_PAGE } from '../../constants';

type movieState = {
    isFetching: boolean,
    isError: boolean,
    data: Array<object>,
    sortBy: 'asc' | 'desc' | 'upper' | 'less' | null
}

const initialState: movieState = {
    isFetching: false,
    isError: false,
    data: [],
    sortBy: null
};

export const moviesSlice = createSlice({
  name: 'movies',
  initialState,
  reducers: {
    getMovies: state => {
      state.isFetching = true;
    },
    getMoviesSuccess: (state, action) => {
      state.isFetching = false;
      state.data = action.payload;
    },
    getMoviesError: state => {
      state.isFetching = false;
      state.isError = true;
    },
    setSort: (state, action) => {
      state.sortBy = action.payload
    },
    setMoviesOnScroll: (state, action) => {
      state.data = state.data.concat(action.payload)
    }
  },
});

export const { setSort } = moviesSlice.actions;
const { getMovies, getMoviesSuccess, getMoviesError, setMoviesOnScroll } = moviesSlice.actions;

export const fetchMoviesList = () => async (dispatch: AppDispatch) => {
  try {
      dispatch(getMovies())
      const movies = await fetchMovies(INITIAL_PAGE)
      
      dispatch(getMoviesSuccess(movies.results))
    } catch (err) {
      dispatch(getMoviesError())
    }
  }

export const fetchMoviesByScroll = (page: number) => async (dispatch: AppDispatch) => {
  try {
    const movies: any = await fetchMovies(page)

    dispatch(setMoviesOnScroll(movies.results))
  } catch (err) {
    dispatch(getMoviesError());
  }
} 

export default moviesSlice.reducer;