import styled from 'styled-components';

export const Container = styled.div`
    padding-top: 25px;
    overflow: auto;
`

export const MoviesContainer = styled.div`
    margin-bottom: 20px;
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    flex-direction: row;
`

export const SpinnerContiner = styled.div`
    display: flex;
    justify-content: center;
`