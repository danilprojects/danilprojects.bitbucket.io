import { connect } from 'react-redux';
import { fetchMoviesList, fetchMoviesByScroll } from './MovieSlice';
import { createSelector } from "reselect";
import orderBy from "lodash/orderBy";
import { RootState } from '../../app/store';

type sortByType = 'asc' | 'desc' | 'upper' | 'less' | null;

type moviesStateType = {
    isError: boolean,
    isFetching: boolean,
    data: Array<any>,
    sortBy: sortByType
};

type movieType = {
    vote_average: string,
    title: string
}

const getIsFetching = (state: RootState): boolean => state.movies.isFetching;
const sortSelector = (state: RootState): sortByType => state.movies.sortBy;
const moviesSelector = (state: RootState): moviesStateType => state.movies;

const getMovies = createSelector(
    [moviesSelector, sortSelector],
    (movies, sortBy) => {       
        if (sortBy) {
            if (sortBy === 'asc' || sortBy === 'desc') {
                return orderBy(
                    movies.data,
                    [(movie: movieType) => movie.title],
                    sortBy
                  );
            }

         if (sortBy === 'less') {
            return orderBy(
                movies.data,
                [(movie: movieType) => movie.vote_average]
              );
         }

         return orderBy(
            movies.data,
            [(movie: movieType) => movie.vote_average]
          ).reverse();
        }

       return movies.data
    }
);

const mapDispatchToProps = {
    fetchMoviesList,
    fetchMoviesByScroll: (page: number) => fetchMoviesByScroll(page)
}

const mapStateToProps = (state: RootState) => ({
    isFetching: getIsFetching(state),
    moviesList: getMovies(state)
});

const connector = connect(mapStateToProps, mapDispatchToProps)

export default connector;