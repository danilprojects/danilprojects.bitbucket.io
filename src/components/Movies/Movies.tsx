import React, { useEffect, useCallback, useState } from 'react';
import Movie from '../common/Movie';
import connector from './MoviesConnector';
import { Container, MoviesContainer, SpinnerContiner } from './style';
import Loader from 'react-loader-spinner';
import InfiniteScroll from "react-infinite-scroll-component";
import { INITIAL_PAGE } from '../../constants';

type MovieType = {
    id: number,
    poster_path: string,
    overview: string,
    title: string
}

type MoviesProps = {
    isFetching: boolean,
    moviesList: Array<MovieType>,
    fetchMoviesList: () => void,
    fetchMoviesByScroll: (page: number) => void
}

const Movies: React.FC<MoviesProps> = ({ fetchMoviesList, moviesList, isFetching, fetchMoviesByScroll }) => {
    const [currentPage, setPage] = useState(INITIAL_PAGE + 1);

    useEffect(() => {
        fetchMoviesList();
    }, [fetchMoviesList])

    const fetchMoviesListByScroll = useCallback(() => {
        setPage(currentPage + 1)

        fetchMoviesByScroll(currentPage)
    }, [currentPage, fetchMoviesByScroll])

    return (
        <Container>
            {
                !isFetching && moviesList
                    ? (
                        <InfiniteScroll
                            dataLength={moviesList.length}
                            next={fetchMoviesListByScroll}
                            hasMore={moviesList.length < 250}
                            loader={
                                <Loader
                                    type="Oval"
                                    color="#800D0D"
                                    height={50}
                                    width={50}
                                />
                            }
                        >
                            <MoviesContainer>
                                {
                                    moviesList.map((movie: MovieType) => (
                                        <Movie
                                            key={movie.id}
                                            id={movie.id}
                                            imgPath={movie.poster_path}
                                            overview={movie.overview}
                                            title={movie.title}
                                        />
                                    ))
                                }
                            </MoviesContainer>
                        </InfiniteScroll>
                    )
                    : (
                        <SpinnerContiner>
                            <Loader
                                visible={isFetching}
                                type="Oval"
                                color="#800D0D"
                                height={50}
                                width={50}
                            />
                        </SpinnerContiner>
                    )
            }
        </Container>
    )
};

export default connector(Movies);