import axios from 'axios';
import { API_KEY, API_URL } from '../constants';

const instance = axios.create({
    baseURL: API_URL,
    responseType: "json",
    params: {
        api_key: API_KEY,
        language: 'en-US'
    },
    headers: { 'X-Requested-With': 'XMLHttpRequest' }
});

export const fetchMovies = (page: number): Promise<any> => {
    const response = instance.get('', {
        params: { page }
    }).then(res => res.data);

    return response
};