import { createGlobalStyle } from 'styled-components';
import styled from 'styled-components';

const size = {
  mobileS: '478px',
  mobileL: '520px',
  tablet: '768px',
  laptopL: '1440px'
}

export const colors = {
  main: '#181818',
  header: '#CE4040',
  footer: '#0e0e0e'
}

export const device = {
  mobileS: `(max-width: ${size.mobileS})`,
  mobileL: `(max-width: ${size.mobileL})`,
  tablet: `(max-width: ${size.tablet})`
};

export const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
  }

  * {
    box-sizing: border-box;
  }
`
export const Container = styled.div`
  margin: auto;
  max-width: ${ size.laptopL};
`

export const AppContainer = styled.div`
  background: ${ colors.main }
`