import { combineReducers } from '@reduxjs/toolkit'
import moviesReduser from '../components/Movies/MovieSlice';
import movieReducer from '../components/common/Movie/MovieSlice';

const rootReducer = combineReducers({
    movies: moviesReduser,
    selectedMovie: movieReducer
});

export default rootReducer;
